// Gravity Forms Plugin
// Written by: Justin Gaskin
/* This nifty plugin allows the form user to go back to previous pages of a multi-page 
split form and re-enter data if corrections need to be made in field data that qualifies 
as valid by GF before it is posted to a database.
*/

jQuery('.gwmpn-page-link').click(function(e){
    e.preventDefault();
	/* register which form is being used */
	var form_page_id = jQuery('.gform_page').attr('id').charAt(11);
	/* register clicked link as subsection to move back to in the form */
	var form_page_num = jQuery(this).attr('href').substr(-1);
	/* count how many pages there are to the form */
	var form_page_lastPage = jQuery('[id*="gform_page_'+form_page_id+'_'+form_page_num+'"]');
	/* switch the end-page-data-preview of the form, while updating the end preview dynamically when user clicks to next page */
	jQuery('[id*="gform_page_'+form_page_id+'_'+form_page_num+'"]').css('display','block');
	jQuery('[id^="gform_page_"]:last').css('display','none');
	if (jQuery(form_page_lastPage).is("td:last-child")) {
		jQuery('.gform__button-lastPage').css('display','block');
	};

	// window.console&&console.log(form_page_num);
	// window.console&&console.log(form_page_id);
	 window.console&&console.log(form_page_lastPage);
});